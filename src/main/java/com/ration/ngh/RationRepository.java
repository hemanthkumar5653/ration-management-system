package com.ration.ngh;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RationRepository extends JpaRepository<RationModel,Long> {

	List<RationModel> findByState(String state);
	
	@Query(value="SELECT sum(revenue) FROM Ration WHERE state=?1",nativeQuery=true)
	long findRevenueByState(String state);
}
