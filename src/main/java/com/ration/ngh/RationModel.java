package com.ration.ngh;

import javax.persistence.*;
import lombok.*;

@Entity
@Table(name="ration")
@Setter
@Getter
@ToString
public class RationModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String address;
	private String city;
	private String country;
	private String name;
	private String state;
	
	@Embedded
    private Revenue revenue;

	
}
