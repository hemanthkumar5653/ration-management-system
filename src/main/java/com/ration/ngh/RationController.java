package com.ration.ngh;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RationController {

	@Autowired
	private RationRepository repo;
	
	@GetMapping("/api/ration")
	public List<RationModel> listAllRationShops() {
		return this.repo.findAll();
	}
	
	@PostMapping("/api/raion")
	public RationModel createRationShop(@RequestBody RationModel ration) {
		return this.repo.save(ration);
	}
	
	@DeleteMapping("/api/ration/{id}")
	public void deleteRationShopById(@PathVariable long id ) {
		this.repo.deleteById(id);
	}
	
	@PutMapping("/api/ration/{id}")
	public RationModel updateRation(@PathVariable long id,@RequestBody RationModel ration) {
		RationModel rat = this.repo.findById(id).orElseThrow(()-> new IllegalArgumentException("nil"));
		rat.setAddress(ration.getAddress());
		rat.setCity(ration.getCity());
		rat.setCountry(ration.getCountry());
		rat.setName(ration.getName());
		rat.setState(ration.getState());
		return this.repo.save(rat);
	}
	
	@GetMapping("/api/ration/{state}")
	public List<RationModel> listRationShopsByState(@PathVariable String state){
		return this.repo.findByState(state);
	}
	
	@GetMapping("/api/ration/revenue/{state}")
	public long getRevenueByState(@PathVariable String state) {
		return this.repo.findRevenueByState(state);
	}
	
	
}
