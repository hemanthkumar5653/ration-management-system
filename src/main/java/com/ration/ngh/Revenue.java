package com.ration.ngh;

import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class Revenue {
  
	private long revenue;
    
}
